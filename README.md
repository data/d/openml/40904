# OpenML dataset: Forest_Cover

https://www.openml.org/d/40904

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The Covertype data which is available at UCI repository. In our experiments, instances from class 2 are considered as normal points and instances from class 4 are anomalies. The anomalies ratios is 0.9%. Instances from the other classes are omitted.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40904) of an [OpenML dataset](https://www.openml.org/d/40904). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40904/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40904/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40904/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

